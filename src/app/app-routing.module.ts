import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './navegacao/home/home.component';
import { NotFoundComponent } from './navegacao/not-found/not-found.component';
import { AcessoNegadoComponent } from './navegacao/acesso-negado/acesso-negado.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'conta',loadChildren: () => import('./conta/conta.module').then(m => m.ContaModule)  },  // Módulo de conta/login
  { path: 'cadastrosBasicos',loadChildren: () => import('./cadastrosBasicos/cadastrosBasicos.module').then(m => m.CadastrosBasicosModule)  },  // Módulo de Cadastros Básicos
  { path: 'nao-encontrado' ,component : NotFoundComponent  }, // 404
  { path: 'acesso-negado' ,component : AcessoNegadoComponent  }, // 403
  { path: '**' ,component : NotFoundComponent  } // 404
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
