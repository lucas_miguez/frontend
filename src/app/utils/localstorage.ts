export class LocalStorageUtils {
     
    public obterUsuario() {
        return JSON.parse(localStorage.getItem('xrsys.user'));
    }

    public salvarDadosLocaisUsuario(response: any) {
        this.salvarTokenUsuario(response.accessToken);
        this.salvarUsuario(response.userToken);
    }

    public limparDadosLocaisUsuario() {
        localStorage.removeItem('xrsys.token');
        localStorage.removeItem('xrsys.user');
    }

    public obterTokenUsuario(): string {
        return localStorage.getItem('xrsys.token');
    }

    public salvarTokenUsuario(token: string) {
        localStorage.setItem('xrsys.token', token);
    }

    public salvarUsuario(user: string) {
        localStorage.setItem('xrsys.user', JSON.stringify(user));
    }

}