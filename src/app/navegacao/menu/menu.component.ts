import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {

  navbarOpen = false;
  show: boolean = false;
 
  isCollapsed: boolean;

  constructor() {

  }
  toggleNavbar() {
    this.isCollapsed = true;
    this.navbarOpen = !this.navbarOpen;
   this.show=false;
  }
  ngOnInit(): void {
  }
}
