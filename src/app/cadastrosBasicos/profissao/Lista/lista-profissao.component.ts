import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { profissao } from '../models/profissao';
import { ProfissaoService } from '../services/profissao.service';


@Component({
  selector: 'app-lista-profissao',
  templateUrl: './lista-profissao.component.html'
})
export class ListaProfissaoComponent implements OnInit {

  public profissaoLista: profissao[];
  errorMessage: string;
  constructor(private spinner: NgxSpinnerService,private profissaoService: ProfissaoService) { }

  ngOnInit(): void 
  {
    this.spinner.show();
    
    this.profissaoService.obterTodos().subscribe
    (
      profissao =>this.profissaoLista = profissao,
      error =>this.errorMessage
    );
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


}
