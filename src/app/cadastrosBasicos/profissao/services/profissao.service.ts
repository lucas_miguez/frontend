import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { BaseService } from 'src/app/services/base.services';
import { profissao } from '../models/profissao';




@Injectable()
export class ProfissaoService extends BaseService
{
    constructor (private http: HttpClient)
    {
        super();
    }

    obterTodos():Observable<profissao[]>
    {
        return this.http
            .get<profissao[]>(this.UrlServiceV1 + "profissao",super.ObterHeaderJson())
            .pipe(
                    map(this.extractData),
                    catchError(this.serviceError)
                 );

    }

    obterPorId(id:string):Observable<profissao>
    {
        return this.http
                .get<profissao>(this.UrlServiceV1+"profissao/"+id,super.ObterHeaderJson())
                .pipe(
                        map(this.extractData),  
                        catchError(super.serviceError)
                        );
    }

    alterarProfissao(esc:profissao ) :Observable<profissao>
    {
            let response = this.http
                        .put(this.UrlServiceV1+"profissao/"+esc.id,esc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }


    adicionarProfissao(esc:profissao ) :Observable<profissao>
    {
            let response = this.http
                        .post(this.UrlServiceV1+"profissao",esc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }




}