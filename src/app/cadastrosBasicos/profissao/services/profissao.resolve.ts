import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { profissao } from '../models/profissao';
import { ProfissaoService } from './profissao.service';

@Injectable()
export class ProfissaoResolve implements Resolve<profissao> {

    constructor(private profissaoService: ProfissaoService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.profissaoService.obterPorId(route.params['id']);
    }
}