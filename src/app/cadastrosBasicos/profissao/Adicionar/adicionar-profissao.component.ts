import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { profissao } from '../models/profissao';
import { ProfissaoService } from '../services/profissao.service';


@Component({
  selector: 'app-adicionar-profissao',
  templateUrl: './adicionar-profissao.component.html'
})
export class AdicionarProfissaoComponent implements OnInit {


  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  
  /** Variáveis **/
  errors: any[] = [];
  profissaoForm: FormGroup;
  usuario: usuario;
  profissao:profissao;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;
  /***************/


  constructor(private fb:FormBuilder,private profissaoService:ProfissaoService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      descricao: {
        required: 'Informe a descrição'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.profissaoForm);
    this.mudancasNaoSalvas = true;
  });
  }



   ngOnInit(): void {
    this.spinner.show();

    this.profissaoForm = this.fb.group
    (
      {
        id: new FormControl({value: '', disabled: true}),
        descricao:['',Validators.required],
        ativo:[false]
      }

    )
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }

  Addprofissao()
  {
    if (this.profissaoForm.dirty && this.profissaoForm.valid)
    {

        this.profissao = Object.assign({},this.profissao,this.profissaoForm.value) ;
        
        
        this.profissaoService.adicionarProfissao(this.profissao).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
  }

  processarSucesso(response: any) {
    this.profissaoForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-profissao']);

    let toast = this.toastr.success('Profissão Adicionado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-profissao']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }

}
