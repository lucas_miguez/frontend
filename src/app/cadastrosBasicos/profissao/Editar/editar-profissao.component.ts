import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { profissao } from '../models/profissao';
import { ProfissaoService } from '../services/profissao.service';

@Component({
  selector: 'app-editar-profissao',
  templateUrl: './editar-profissao.component.html'
})
export class EditarProfissaoComponent implements OnInit,AfterViewInit {


  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  
  /** Variáveis **/
  errors: any[] = [];
  profissaoForm: FormGroup;
  usuario: usuario;
  profissao:profissao;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;
  /***************/

  constructor(private fb:FormBuilder,private profissaoService:ProfissaoService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      descricao: {
        required: 'Informe a descrição'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);

    this.profissao = this.route.snapshot.data['profissao'];
    
    this.id = this.profissao.id;
  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.profissaoForm);
    this.mudancasNaoSalvas = true;
  });
  }
  
  ngOnInit(): void {
    this.spinner.show();

    this.profissaoForm = this.fb.group
    (
      {
        id: new FormControl({value: '', disabled: true}),
        descricao:[Validators.required],
        ativo:[false]
      }

    )

    this.preencherForm();


    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }

  preencherForm() {
    this.profissaoForm.patchValue({
      id: this.profissao.id,
      descricao: this.profissao.descricao,
      ativo:this.profissao.ativo
    });
  }


 
  Editarprofissao()
  {
    if (this.profissaoForm.dirty && this.profissaoForm.valid)
    {
        this.profissao = Object.assign({},this.profissao,this.profissaoForm.value) ;
        
        console.log(this.profissao);
        
        this.profissaoService.alterarProfissao(this.profissao).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
    else
    {
      this.toastr.info('Realize alguma mudança no formulário!', 'Ops');
    }
  }

  processarSucesso(response: any) {
    this.profissaoForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-profissao']);

    let toast = this.toastr.success('Profissão Alterado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-profissao']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }


}

