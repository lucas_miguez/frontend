import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastrosBasicosAppComponent } from './cadastrosBasicos.app.component';
import { TipoDocumentoComponent } from './tipo-documento/tipo-documento.component';
import { ListaTipoDocumentoComponent } from './tipo-documento/Lista/lista-tipo-documento.component';
import { EditarTipoDocumentoComponent } from './tipo-documento/Editar/editar-tipo-documento.component';
import { TipoDocumentoResolve } from './tipo-documento/services/tipo-documento.resolve';
import { AdicionarTipoDocumentoComponent } from './tipo-documento/Adicionar/adicionar-tipo-documento.component';
import { ListaEscolaridadeComponent } from './escolaridade/Lista/lista-escolaridade.component';
import { EditarEscolaridadeComponent } from './escolaridade/Editar/editar-escolaridade.component';
import { EscolaridadeResolve } from './escolaridade/services/escolaridade.resolve';
import { AdicionarEscolaridadeComponent } from './escolaridade/Adicionar/adicionar-escolaridade.component';
import { ListaProfissaoComponent } from './profissao/Lista/lista-profissao.component';
import { ProfissaoComponent } from './profissao/profissao.component';
import { EditarProfissaoComponent } from './profissao/Editar/editar-profissao.component';
import { ProfissaoResolve } from './profissao/services/profissao.resolve';
import { AdicionarProfissaoComponent } from './profissao/Adicionar/adicionar-profissao.component';
import { EditarPessoaComponent } from './pessoa/editar-pessoa/editar-pessoa.component';
import { ListaPessoaComponent } from './pessoa/lista-pessoa/lista-pessoa.component';
import { PessoaComponent } from './pessoa/pessoa.component';
import { PessoaResolve } from './pessoa/services/pessoa.resolve';
import { AdicionarPessoaComponent } from './pessoa/adicionar-pessoa/adicionar-pessoa.component';


const contaRouterConfig: Routes = [
    {
        path: '', component: CadastrosBasicosAppComponent,
        children: [
            
            // ## Escolaridade ##
            { path: 'lista-escolaridade', component:  ListaEscolaridadeComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },
            { path: 'editar-escolaridade/:id', component: EditarEscolaridadeComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ ,
                resolve:
                {
                    escolaridade: EscolaridadeResolve
                }
            },
            { path: 'adicionar-escolaridade', component: AdicionarEscolaridadeComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },
            //###################
            
            // ## Tipo Documento ##
            { path: 'tipo-documento', component: TipoDocumentoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },
            { path: 'lista-tipo-documento', component: ListaTipoDocumentoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },//,
            { path: 'editar-tipo-documento/:id', component: EditarTipoDocumentoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ ,
                resolve:
                {
                    tipodocumento: TipoDocumentoResolve
                }
            },
            { path: 'adicionar-tipo-documento', component: AdicionarTipoDocumentoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },//,
            //#######################
            
            // ## Profissão ##
              { path: 'profissao', component: ProfissaoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },
              { path: 'lista-profissao', component: ListaProfissaoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },//,
              { path: 'editar-profissao/:id', component: EditarProfissaoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ ,
                  resolve:
                  {
                      profissao: ProfissaoResolve
                  }
              },
              { path: 'adicionar-profissao', component: AdicionarProfissaoComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },//,
              //#######################

              // ## Pessoa ##
              { path: 'pessoa', component: PessoaComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },
              { path: 'lista-pessoa', component: ListaPessoaComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ },//,
              { path: 'editar-pessoa/:id', component: EditarPessoaComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ ,
                  resolve:
                  {
                      pessoa: PessoaResolve
                  }
              },
              { path: 'adicionar-pessoa', component: AdicionarPessoaComponent/*, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard]*/ }//,
              //#######################
        ]
    }
];



@NgModule({
    imports: [
        RouterModule.forChild(contaRouterConfig)
    ],
    exports: [RouterModule]
})
export class CadastrosBasicosRoutingModule { }