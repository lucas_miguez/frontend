import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { Pessoa } from '../models/pessoa';
import { PessoaService } from '../services/Pessoa.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-editar-pessoa',
  templateUrl: './editar-pessoa.component.html'
})
export class EditarPessoaComponent implements  OnInit,AfterViewInit {


  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  /** Variáveis **/
  errors: any[] = [];
  PessoaForm: FormGroup;
  usuario: usuario;
  pessoa:Pessoa;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;
  /***************/


  constructor(private fb:FormBuilder,private pessoaService:PessoaService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      nome: {
        required: 'Informe o nome'
      },
      cpfCnpj: {
        required: 'Informe o Cpf/Cnpj'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);

    this.pessoa = this.route.snapshot.data['pessoa'];
    
    this.id = this.pessoa.id;

  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.PessoaForm);
    this.mudancasNaoSalvas = true;
  });
  }


  ngOnInit(): void {
    this.spinner.show();

    this.PessoaForm = this.fb.group
    (
      {
        id: new FormControl({value: '', disabled: true}),
        nome:[Validators.required],
        cpfCnpj:[Validators.required],
        nomeFantasia:[],
        tipoPessoa:[Validators.required],
        rg:[]
      }
    )

    this.preencherForm();
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }

  preencherTipoPessoa(p_tipopessoa:string) :string
  { 
    switch(p_tipopessoa) 
    {
      case "F":
        {
          return "Física";
          break;
        }
        case "J":
          {
            return "Jurídica";
            break;
          }
    }
  }

  preencherForm() {

    //console.log('Tipo Pessoa')  
    //console.log(this.preencherTipoPessoa(this.pessoa.tipoPessoa));

    this.PessoaForm.patchValue({
      id: this.pessoa.id,
      nome: this.pessoa.nome,
      nomeFantasia:this.pessoa.nomeFantasia,
      tipoPessoa:this.preencherTipoPessoa(this.pessoa.tipoPessoa),
      cpfCnpj : this.pessoa.cpfCnpj,
      rg: this.pessoa.rg
    });
  }

  EditarPessoa()  
  {
    if (this.PessoaForm.dirty && this.PessoaForm.valid)
    {
        this.pessoa = Object.assign({},this.pessoa,this.PessoaForm.value) ;
        
        console.log(this.pessoa);
        
        this.pessoaService.alterarPessoa(this.pessoa).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
  }

  processarSucesso(response: any) {
    this.PessoaForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-pessoa']);

    let toast = this.toastr.success('Pessoa Alterado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-pessoa']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }




}
