
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { BaseService } from 'src/app/services/base.services';
import { Pessoa } from '../models/pessoa';



@Injectable()
export class PessoaService extends BaseService
{
    constructor (private http: HttpClient)
    {
        super();
    }

    obterTodos():Observable<Pessoa[]>
    {
        return this.http
            .get<Pessoa[]>(this.UrlServiceV1 + "pessoa",super.ObterHeaderJson())
            .pipe(
                    map(this.extractData),
                    catchError(this.serviceError)
                 );

    }

    obterPorId(id:string):Observable<Pessoa>
    {
        return this.http
                .get<Pessoa>(this.UrlServiceV1+"pessoa/"+id,super.ObterHeaderJson())
                .pipe(
                        map(this.extractData),  
                        catchError(super.serviceError)
                        );
    }

    alterarPessoa(pess:Pessoa ) :Observable<Pessoa>
    {
            let response = this.http
                        .put(this.UrlServiceV1+"pessoa/"+pess.id,pess,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }


    adicionarPessoa(pess:Pessoa ) :Observable<Pessoa>
    {
            let response = this.http
                        .post(this.UrlServiceV1+"pessoa",pess,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }




}