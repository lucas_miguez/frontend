import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Pessoa } from '../models/pessoa';
import { PessoaService } from './Pessoa.service';





@Injectable()
export class PessoaResolve implements Resolve<Pessoa> {

    constructor(private pessoaService: PessoaService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.pessoaService.obterPorId(route.params['id']);
    }
}