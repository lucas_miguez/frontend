import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators, AbstractControl } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { Pessoa } from '../models/pessoa';
import { PessoaService } from '../services/Pessoa.service';
import { stringify } from 'querystring';
import { MASKS, NgBrazilValidators } from 'ng-brazil';


@Component({
  selector: 'app-adicionar-pessoa',
  templateUrl: './adicionar-pessoa.component.html'
  
})
export class AdicionarPessoaComponent implements OnInit,AfterViewInit {
  
  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  /** Variáveis **/
  public MASKS = MASKS;
  public mascaraTipoPessoa = MASKS.cpf.textMask;
  errors: any[] = [];
  PessoaForm: FormGroup;
  usuario: usuario;
  pessoa:Pessoa;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;  
  textoDocumento :string = 'CPF (requerido)';
  
  /***************/


  constructor(private fb:FormBuilder,private pessoaService:PessoaService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      nome: {
        required: 'Informe o nome'
      },
      cpfCnpj: {
        required: 'Informe o Cpf/Cnpj'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);

  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.PessoaForm);
    this.mudancasNaoSalvas = true;
    });

    this.tipoPessoaForm().valueChanges
      .subscribe(() => {
        this.trocarValidacaoDocumento();
        this.configurarElementosValidacao();
        this.validarFormulario();
      });

    this.configurarElementosValidacao();
  }

  configurarElementosValidacao() {
    let controlBlurs: Observable<any>[] = this.formInputElements
      .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

    merge(...controlBlurs).subscribe(() => {
      this.validarFormulario();
    });
  }

  validarFormulario() {
    this.displayMessage = this.genericValidator.processarMensagens(this.PessoaForm);
    this.mudancasNaoSalvas = true;
  }

  ngOnInit(): void {
    this.spinner.show();

    this.PessoaForm = this.fb.group
    (
      {
        id: new FormControl
            (
               {value: '', disabled: true}
            ),
        nome:['',Validators.required],
        dataNascFund:[Validators.required],
        cpfCnpj:['',Validators.required],
        nomeFantasia:[],
        tipoPessoa:['',Validators.required],
        rg:[]
      }
    )
    this.PessoaForm.patchValue({ tipoPessoa: 'F', ativo: true });


    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  tipoPessoaForm(): AbstractControl {
    return this.PessoaForm.get('tipoPessoa');
  }

  documentoCpfCnpj(): AbstractControl {
    return this.PessoaForm.get('cpfCnpj');
  }

  trocarValidacaoDocumento() {
    if (this.tipoPessoaForm().value === "F") {
      this.documentoCpfCnpj().clearValidators();
      this.documentoCpfCnpj().setValidators([Validators.required, NgBrazilValidators.cpf]);
      this.textoDocumento = "CPF (requerido)";
      this.mascaraTipoPessoa = MASKS.cpf.textMask;
    }
    else {
      this.documentoCpfCnpj().clearValidators();
      this.documentoCpfCnpj().setValidators([Validators.required, NgBrazilValidators.cnpj]);
      this.textoDocumento = "CNPJ (requerido)";
      this.mascaraTipoPessoa = MASKS.cnpj.textMask;
    }
  }
  
  AdicionarPessoa()  
  {
    if (this.PessoaForm.dirty && this.PessoaForm.valid)
    {
        this.pessoa = Object.assign({},this.pessoa,this.PessoaForm.value) ;
        
        console.log(this.pessoa);
        
        this.pessoaService.adicionarPessoa(this.pessoa).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
  }



  processarSucesso(response: any) {
    this.PessoaForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-pessoa']);

    let toast = this.toastr.success('Pessoa Alterado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-pessoa']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }


}
