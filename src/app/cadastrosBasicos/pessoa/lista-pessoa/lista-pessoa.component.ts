import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Pessoa } from '../models/pessoa';
import { PessoaService } from '../services/Pessoa.service';

@Component({
  selector: 'app-lista-pessoa',
  templateUrl: './lista-pessoa.component.html'
})
export class ListaPessoaComponent implements OnInit {

  public PessoaLista: Pessoa[];
  errorMessage: string;


  constructor(private spinner: NgxSpinnerService,private pessoaService: PessoaService) {}

  ngOnInit(): void 
  {
    this.spinner.show();
    
    this.pessoaService.obterTodos().subscribe
    (
      pessoa =>this.PessoaLista = pessoa,
      error =>this.errorMessage
    );
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }

}
