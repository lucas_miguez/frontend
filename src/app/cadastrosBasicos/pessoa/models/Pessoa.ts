import { escolaridade } from '../../escolaridade/models/escolaridade';
import { profissao } from '../../profissao/models/profissao';
import { EstadoCivil } from './EstadoCivil';

export class Pessoa
{
        id:string;
        nome:string;
        nomeFantasia:string;
        tipoPessoa:string;
        cpfCnpj:string;
        rg:string;
        dataNascFund:Date;
        sexo:string;
        estadoCivil:EstadoCivil;
        escolaridade:escolaridade;
        profissao:profissao;
        email:string;
        ativo:Boolean;

}


