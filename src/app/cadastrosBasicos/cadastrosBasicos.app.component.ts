import { Component } from '@angular/core';

@Component({
  selector: 'cadastrosBasicos-app-root',
  template: '<router-outlet></router-outlet>'
})
export class CadastrosBasicosAppComponent { }
