import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { escolaridade } from '../models/escolaridade';
import { EscolaridadeService } from '../services/escolaridade.service';


@Component({
  selector: 'app-lista-escolaridade',
  templateUrl: './lista-escolaridade.component.html'
})
export class ListaEscolaridadeComponent implements OnInit {

  public escolaridadeLista: escolaridade[];
  errorMessage: string;
  constructor(private spinner: NgxSpinnerService,private escolaridadeService: EscolaridadeService) { }

  ngOnInit(): void 
  {
    this.spinner.show();
    
    this.escolaridadeService.obterTodos().subscribe
    (
      escolaridade =>this.escolaridadeLista = escolaridade,
      error =>this.errorMessage
    );
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


}
