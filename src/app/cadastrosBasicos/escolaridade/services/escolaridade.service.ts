import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { BaseService } from 'src/app/services/base.services';
import { escolaridade } from '../models/escolaridade';



@Injectable()
export class EscolaridadeService extends BaseService
{
    constructor (private http: HttpClient)
    {
        super();
    }

    obterTodos():Observable<escolaridade[]>
    {
        return this.http
            .get<escolaridade[]>(this.UrlServiceV1 + "escolaridade",super.ObterHeaderJson())
            .pipe(
                    map(this.extractData),
                    catchError(this.serviceError)
                 );

    }

    obterPorId(id:string):Observable<escolaridade>
    {
        return this.http
                .get<escolaridade>(this.UrlServiceV1+"escolaridade/"+id,super.ObterHeaderJson())
                .pipe(
                        map(this.extractData),  
                        catchError(super.serviceError)
                        );
    }

    alterarEscolaridade(esc:escolaridade ) :Observable<escolaridade>
    {
            let response = this.http
                        .put(this.UrlServiceV1+"escolaridade/"+esc.id,esc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }


    adicionarEscolaridade(esc:escolaridade ) :Observable<escolaridade>
    {
            let response = this.http
                        .post(this.UrlServiceV1+"escolaridade",esc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }




}