import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { escolaridade } from '../models/escolaridade';
import { EscolaridadeService } from './escolaridade.service';




@Injectable()
export class EscolaridadeResolve implements Resolve<escolaridade> {

    constructor(private escolaridadeService: EscolaridadeService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.escolaridadeService.obterPorId(route.params['id']);
    }
}