import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { escolaridade } from '../models/escolaridade';
import { EscolaridadeService } from '../services/escolaridade.service';


@Component({
  selector: 'app-adicionar-escolaridade',
  templateUrl: './adicionar-escolaridade.component.html'
})
export class AdicionarEscolaridadeComponent implements OnInit {


  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  
  /** Variáveis **/
  errors: any[] = [];
  EscolaridadeForm: FormGroup;
  usuario: usuario;
  escolaridade:escolaridade;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;
  /***************/


  constructor(private fb:FormBuilder,private escolaridadeService:EscolaridadeService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      descricao: {
        required: 'Informe a descrição'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);
  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.EscolaridadeForm);
    this.mudancasNaoSalvas = true;
  });
  }



   ngOnInit(): void {
    this.spinner.show();

    this.EscolaridadeForm = this.fb.group
    (
      {
        id: new FormControl({value: '', disabled: true}),
        descricao:['',Validators.required],
        ativo:[false]
      }

    )
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }

  AddEscolaridade()
  {
    if (this.EscolaridadeForm.dirty && this.EscolaridadeForm.valid)
    {

        this.escolaridade = Object.assign({},this.escolaridade,this.EscolaridadeForm.value) ;
        console.log(this.escolaridade);
        
        this.escolaridadeService.adicionarEscolaridade(this.escolaridade).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
  }

  processarSucesso(response: any) {
    this.EscolaridadeForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-escolaridade']);

    let toast = this.toastr.success('Escolaridade Adicionado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-escolaridade']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }

}
