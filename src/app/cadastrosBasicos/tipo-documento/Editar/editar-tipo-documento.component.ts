import { Component, OnInit, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { FormGroup, FormBuilder, FormControlName, FormControl, Validators } from '@angular/forms';
import { usuario } from 'src/app/conta/models/usuario';
import { TipoDocumentoService } from '../services/tipo-documento.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { Observable, fromEvent, merge } from 'rxjs';
import { tipodocumento } from '../models/tipodocumento';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-editar-tipo-documento',
  templateUrl: './editar-tipo-documento.component.html'
})
export class EditarTipoDocumentoComponent implements OnInit,AfterViewInit {


  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

  
  /** Variáveis **/
  errors: any[] = [];
  TipoDocumentoForm: FormGroup;
  usuario: usuario;
  tipodocumento:tipodocumento;
  mudancasNaoSalvas: boolean;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  id:string;
  /***************/

  constructor(private fb:FormBuilder,private tipoDocumentoService:TipoDocumentoService,private router:Router,private toastr:ToastrService, private spinner: NgxSpinnerService,private route: ActivatedRoute) 
  {
    this.validationMessages = {
      descricao: {
        required: 'Informe a descrição'
      }
    }

    this.genericValidator = new GenericValidator(this.validationMessages);

    this.tipodocumento = this.route.snapshot.data['tipodocumento'];
    

    this.id = this.tipodocumento.id;


  }

  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.TipoDocumentoForm);
    this.mudancasNaoSalvas = true;
  });
  }
  
  ngOnInit(): void {
    this.spinner.show();

    this.TipoDocumentoForm = this.fb.group
    (
      {
        id: new FormControl({value: '', disabled: true}),
        descricao:[Validators.required],
        ativo:[]
      }

    )

    this.preencherForm();


    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

  }

  preencherForm() {
    this.TipoDocumentoForm.patchValue({
      id: this.tipodocumento.id,
      descricao: this.tipodocumento.descricao,
      ativo:this.tipodocumento.ativo
    });
  }


 
  EditarTipoDocumento()
  {
    if (this.TipoDocumentoForm.dirty && this.TipoDocumentoForm.valid)
    {
        //this.usuarioTrocaSenha = Object.assign({},this.usuarioTrocaSenha,this.trocarSenhaForm.value) ;
        //let novaSenha = this.TipoDocumentoForm.get('confirmPassword').value;
        //this.usuarioTrocaSenha = new usuarioTrocaSenha(this.usuarioCompleto.id,novaSenha)
        

        this.tipodocumento = Object.assign({},this.tipodocumento,this.TipoDocumentoForm.value) ;
        
        console.log(this.tipodocumento);
        
        this.tipoDocumentoService.alterarTipoDocumento(this.tipodocumento).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
    else
    {
      this.toastr.info('Realize alguma mudança no formulário!', 'Ops');
    }
  }

  processarSucesso(response: any) {
    this.TipoDocumentoForm.reset();
    this.errors = [];

    this.router.navigate(['/cadastrosBasicos/lista-tipo-documento']);

    let toast = this.toastr.success('Tipo de Documento Alterado com Sucesso!', '',{timeOut:1000});
    
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/cadastrosBasicos/lista-tipo-documento']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }


}

