import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { BaseService } from 'src/app/services/base.services';
import { tipodocumento } from '../models/tipodocumento';


@Injectable()
export class TipoDocumentoService extends BaseService
{
    constructor (private http: HttpClient)
    {
        super();
    }

    obterTodos():Observable<tipodocumento[]>
    {
        return this.http
            .get<tipodocumento[]>(this.UrlServiceV1 + "tipodocumento",super.ObterHeaderJson())
            .pipe(
                    map(this.extractData),
                    catchError(this.serviceError)
                 );

    }

    obterPorId(id:string):Observable<tipodocumento>
    {
        return this.http
                .get<tipodocumento>(this.UrlServiceV1+"tipodocumento/"+id,super.ObterHeaderJson())
                .pipe(
                        map(this.extractData),  
                        catchError(super.serviceError)
                        );
    }

    alterarTipoDocumento(tpdoc:tipodocumento ) :Observable<tipodocumento>
    {
            let response = this.http
                        .put(this.UrlServiceV1+"tipodocumento/"+tpdoc.id,tpdoc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }


    adicionarTipoDocumento(tpdoc:tipodocumento ) :Observable<tipodocumento>
    {
            let response = this.http
                        .post(this.UrlServiceV1+"tipodocumento",tpdoc,super.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }




}