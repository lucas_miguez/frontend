import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { tipodocumento } from '../models/tipodocumento';
import { TipoDocumentoService } from './tipo-documento.service';


@Injectable()
export class TipoDocumentoResolve implements Resolve<tipodocumento> {

    constructor(private tipodocumentoService: TipoDocumentoService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.tipodocumentoService.obterPorId(route.params['id']);
    }
}