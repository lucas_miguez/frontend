import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { TipoDocumentoService } from '../services/tipo-documento.service';
import { tipodocumento } from '../models/tipodocumento';

@Component({
  selector: 'app-lista-tipo-documento',
  templateUrl: './lista-tipo-documento.component.html'
})
export class ListaTipoDocumentoComponent implements OnInit {

  public tipoDocumentoLista: tipodocumento[];
  errorMessage: string;
  constructor(private spinner: NgxSpinnerService,private tpDocumentoService: TipoDocumentoService) { }

  ngOnInit(): void 
  {
    this.spinner.show();
    
    this.tpDocumentoService.obterTodos().subscribe
    (
      tipodocumento =>this.tipoDocumentoLista = tipodocumento,
      error =>this.errorMessage
    );
    
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


}
