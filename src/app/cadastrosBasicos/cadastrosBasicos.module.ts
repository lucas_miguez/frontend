import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CustomFormsModule } from 'ngx-custom-validators'
import { NgxSpinnerModule } from 'ngx-spinner';
import { CadastrosBasicosAppComponent } from './cadastrosBasicos.app.component';
import { CadastrosBasicosRoutingModule } from './cadastrosBasicos.route';
import { TipoDocumentoComponent } from './tipo-documento/tipo-documento.component';
import { ListaTipoDocumentoComponent } from './tipo-documento/Lista/lista-tipo-documento.component';
import { TipoDocumentoService } from './tipo-documento/services/tipo-documento.service';
import { EditarTipoDocumentoComponent } from './tipo-documento/Editar/editar-tipo-documento.component';
import { TipoDocumentoResolve } from './tipo-documento/services/tipo-documento.resolve';
import { AdicionarTipoDocumentoComponent } from './tipo-documento/Adicionar/adicionar-tipo-documento.component';
import { ListaEscolaridadeComponent } from './escolaridade/Lista/lista-escolaridade.component';
import { EscolaridadeComponent } from './escolaridade/escolaridade.component';
import { EscolaridadeService } from './escolaridade/services/escolaridade.service';
import { EscolaridadeResolve } from './escolaridade/services/escolaridade.resolve';
import { EditarEscolaridadeComponent } from './escolaridade/Editar/editar-escolaridade.component';
import { AdicionarEscolaridadeComponent } from './escolaridade/Adicionar/adicionar-escolaridade.component';
import { ProfissaoComponent } from './profissao/profissao.component';
import { AdicionarProfissaoComponent } from './profissao/Adicionar/adicionar-profissao.component';
import { EditarProfissaoComponent } from './profissao/Editar/editar-profissao.component';
import { ListaProfissaoComponent } from './profissao/Lista/lista-profissao.component';
import { ProfissaoService } from './profissao/services/profissao.service';
import { ProfissaoResolve } from './profissao/services/profissao.resolve';
import { AdicionarPessoaComponent } from './pessoa/adicionar-pessoa/adicionar-pessoa.component';
import { EditarPessoaComponent } from './pessoa/editar-pessoa/editar-pessoa.component';
import { ListaPessoaComponent } from './pessoa/lista-pessoa/lista-pessoa.component';
import { PessoaComponent } from './pessoa/pessoa.component';
import { PessoaService } from './pessoa/services/Pessoa.service';
import { PessoaResolve } from './pessoa/services/pessoa.resolve';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgBrazil } from 'ng-brazil';
import { TextMaskModule } from 'angular2-text-mask';


@NgModule({
    declarations: 
      [
        CadastrosBasicosAppComponent,
        TipoDocumentoComponent,
        ListaTipoDocumentoComponent,
        EditarTipoDocumentoComponent,
        AdicionarTipoDocumentoComponent,
        ListaEscolaridadeComponent,
        EscolaridadeComponent,
        EditarEscolaridadeComponent,
        AdicionarEscolaridadeComponent,
        ProfissaoComponent,
        AdicionarProfissaoComponent,
        EditarProfissaoComponent,
        ListaProfissaoComponent,
        AdicionarPessoaComponent,
        EditarPessoaComponent,
        ListaPessoaComponent,
        PessoaComponent
      ],
    imports: [
      CommonModule,
      RouterModule,
      CadastrosBasicosRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      CustomFormsModule,
      NgxSpinnerModule,
      TextMaskModule,
      NgBrazil,
      BsDatepickerModule.forRoot()
    ],
    providers:[
                TipoDocumentoService,
                TipoDocumentoResolve,
                EscolaridadeService,
                EscolaridadeResolve,
                ProfissaoService,
                ProfissaoResolve,
                PessoaResolve,
                PessoaService//contaService,ContaGuard,UsuarioGuard,UsuarioResolve
              ]
  })
  export class CadastrosBasicosModule { }
  