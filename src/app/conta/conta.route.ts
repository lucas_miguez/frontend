import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ContaAppComponent } from './conta.app.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ContaGuard } from './services/conta.guard';
import { ListaUsuariosComponent } from './lista/lista-usuarios/lista-usuarios.component';
import { UsuarioGuard } from './services/usuario.guard';
import { TrocarsenhaComponent } from './trocarsenha/trocarsenha/trocarsenha.component';
import { UsuarioResolve } from './services/usuario.resolve';

const contaRouterConfig: Routes = [
    {
        path: '', component: ContaAppComponent,
        children: [
            { path: 'cadastro', component: CadastroComponent, canActivate: [UsuarioGuard], canDeactivate: [UsuarioGuard] },
            { path: 'login', component: LoginComponent,canActivate: [ContaGuard]},
            { path: 'lista-usuarios', component: ListaUsuariosComponent, canActivate: [UsuarioGuard]   },
            { path: 'trocarsenha/:id' ,component:TrocarsenhaComponent ,
                    resolve:
                    {
                        usuarioCompleto: UsuarioResolve
                    }
            }
        ]
    }
];



@NgModule({
    imports: [
        RouterModule.forChild(contaRouterConfig)
    ],
    exports: [RouterModule]
})
export class ContaRoutingModule { }