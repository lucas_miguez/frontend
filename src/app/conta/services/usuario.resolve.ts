import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { usuario } from '../models/usuario';
import { contaService } from './conta.service';
import { UsuarioCompleto } from '../models/usuarioCompleto';


@Injectable()
export class UsuarioResolve implements Resolve<UsuarioCompleto> {

    constructor(private usuarioService: contaService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.usuarioService.obterPorId(route.params['id']);
    }
}