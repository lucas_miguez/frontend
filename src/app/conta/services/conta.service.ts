import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { usuario } from '../models/usuario';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { BaseService } from 'src/app/services/base.services';
import { UsuarioCompleto } from '../models/usuarioCompleto';
import { usuarioTrocaSenha } from '../models/usuarioTrocaSenha';

@Injectable()
export class contaService extends BaseService
{
    constructor (private http: HttpClient)
    {
        super();
    }


    trocarSenha(usuario:usuarioTrocaSenha ) :Observable<usuarioTrocaSenha>
    {
            let response = this.http
                        .post(this.UrlServiceV1+'TrocarSenha',usuario,this.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response; 
    }


    obterPorId(id:string):Observable<UsuarioCompleto>
    {
        return this.http
                .get<UsuarioCompleto>(this.UrlServiceV1+"usuario/"+id,super.ObterHeaderJson())
                .pipe(
                        map(this.extractData),  
                        catchError(super.serviceError)
                        );
    }

    obterTodos():Observable<usuario[]>
    {
        return this.http
            .get<usuario[]>(this.UrlServiceV1 + "listar-usuarios",super.ObterHeaderJson())
            .pipe(
                    map(this.extractData),
                    catchError(this.serviceError)
                 );

    }

    registrarUsuario(usuario:usuario ) :Observable<usuario>
    {
            let response = this.http
                        .post(this.UrlServiceV1+'nova-conta',usuario,this.ObterHeaderJson())
                        .pipe
                        (
                            map(this.extractData),
                            catchError(this.serviceError)
                        );
            return response;
    }

    login(usurio:usuario ):Observable<usuario>
    {
        let response = this.http
        .post(this.UrlServiceV1+'entrar',usurio,this.ObterHeaderJson())
        .pipe
        (
            map(this.extractData),
            catchError(this.serviceError)
        );
        return response;
    }
}