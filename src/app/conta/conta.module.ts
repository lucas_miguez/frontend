import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CustomFormsModule } from 'ngx-custom-validators'

import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';

import { ContaRoutingModule } from './conta.route';
import { ContaAppComponent } from './conta.app.component';
import { contaService } from './services/conta.service';
import { ContaGuard } from './services/conta.guard';
import { ListaUsuariosComponent } from './lista/lista-usuarios/lista-usuarios.component';
import { TrocarsenhaComponent } from './trocarsenha/trocarsenha/trocarsenha.component';
import { UsuarioGuard } from './services/usuario.guard';
import { UsuarioResolve } from './services/usuario.resolve';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: 
    [
      ContaAppComponent,
      CadastroComponent,
      LoginComponent,
      ListaUsuariosComponent,
      ListaUsuariosComponent,
      TrocarsenhaComponent
    ],
  imports: [
    CommonModule,
    RouterModule,
    ContaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    NgxSpinnerModule
  ],
  providers:[contaService,ContaGuard,UsuarioGuard,UsuarioResolve]
})
export class ContaModule { }
