import { Component, OnInit, AfterViewInit, ElementRef, ViewChildren, ComponentFactoryResolver } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormControlName } from '@angular/forms';

import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { CustomValidators } from 'ngx-custom-validators';
import { merge, Observable, fromEvent } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService, Toast } from 'ngx-toastr';
import { usuario } from '../../models/usuario';
import { contaService } from '../../services/conta.service';
import { UsuarioCompleto } from '../../models/usuarioCompleto';
import { usuarioTrocaSenha } from '../../models/usuarioTrocaSenha';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-trocarsenha',
  templateUrl: './trocarsenha.component.html'
})
export class TrocarsenhaComponent implements  OnInit,AfterViewInit {

  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
  
  errors: any[] = [];
  trocarSenhaForm: FormGroup;
  usuario: usuario;
  usuarioCompleto: UsuarioCompleto;
  mudancasNaoSalvas: boolean;
  email:string;
  usuarioTrocaSenha:usuarioTrocaSenha;
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  
  

  constructor(private fb:FormBuilder,private contaService:contaService,private router:Router,private toastr:ToastrService,private route: ActivatedRoute,private spinner: NgxSpinnerService) 
  {
    this.validationMessages = {
      password: { 
        required: 'Informe a senha',
        rangeLength: 'A senha deve possuir entre 6 e 15 caracteres'
      },
      confirmPassword: {
        required: 'Informe a senha novamente',
        rangeLength: 'A senha deve possuir entre 6 e 15 caracteres',
        equalTo: 'As senhas não conferem'
      }
    };  
    this.genericValidator = new GenericValidator(this.validationMessages);
    
    this.usuarioCompleto = this.route.snapshot.data['usuarioCompleto'];
    
    this.email = this.usuarioCompleto.email;

  }
  




  ngAfterViewInit(): void {
    
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.trocarSenhaForm);
    this.mudancasNaoSalvas = true;
  
  });
  }

  ngOnInit(): void {
    this.spinner.show();
    let senha = new FormControl('', [Validators.required, CustomValidators.rangeLength([6, 15])]);
    let senhaConfirm = new FormControl('', [Validators.required, CustomValidators.rangeLength([6, 15]), CustomValidators.equalTo(senha)]);


    this.trocarSenhaForm = this.fb.group(
      {
        email:new FormControl({value: '', disabled: true}),
        password:senha,
        confirmPassword:senhaConfirm
      })

      this.preencherForm();
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 1000);

  }
  
  preencherForm() {
    this.trocarSenhaForm.patchValue({
      email: this.usuarioCompleto.email
    });
  }


  trocarSenha()
  {
    if (this.trocarSenhaForm.dirty && this.trocarSenhaForm.valid)
    {
        //this.usuarioTrocaSenha = Object.assign({},this.usuarioTrocaSenha,this.trocarSenhaForm.value) ;
        let novaSenha = this.trocarSenhaForm.get('confirmPassword').value;
        this.usuarioTrocaSenha = new usuarioTrocaSenha(this.usuarioCompleto.id,novaSenha)
        
        
        this.contaService.trocarSenha(this.usuarioTrocaSenha).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        this.mudancasNaoSalvas = false;
    }
  }

  processarSucesso(response: any) {
    this.trocarSenhaForm.reset();
    this.errors = [];

    this.router.navigate(['/home']);

    let toast = this.toastr.success('Senha Alterada com Sucesso!', '');
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/home']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }
    



}
