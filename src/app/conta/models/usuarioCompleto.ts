export interface UsuarioCompleto
{
    id:string,
    userName: string,
    normalizedUserName: string,
    email: string,
    normalizedEmail: string,
    emailConfirmed: true//,
    // passwordHash: string,
    // securityStamp: string,
    // concurrencyStamp: string,
    // phoneNumber: number,
    // phoneNumberConfirmed: boolean,
    // twoFactorEnabled: boolean,
    // lockoutEnd: date,
    // lockoutEnabled: true,
    // accessFailedCount: 0

}