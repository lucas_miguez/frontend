import { Component, OnInit } from '@angular/core';
import { usuario } from '../../models/usuario';
import { contaService } from '../../services/conta.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html'
})
export class ListaUsuariosComponent implements OnInit {

  public usuarios: usuario[];
  errorMessage: string;

  constructor(private ctaService: contaService,
                private spinner: NgxSpinnerService)
  { }

  ngOnInit(): void {
    this.spinner.show();
    
    this.ctaService.obterTodos().subscribe
    (
      usuarios =>this.usuarios = usuarios,
      error =>this.errorMessage
    )

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 1000);

  }

}
