import { Component, OnInit, AfterViewInit, ElementRef, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormControlName } from '@angular/forms';
import { usuario } from '../models/usuario';
import { contaService } from '../services/conta.service';
import { ValidationMessages, GenericValidator, DisplayMessage } from 'src/app/utils/generic-form-validation';
import { CustomValidators } from 'ngx-custom-validators';
import { merge, Observable, fromEvent } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService, Toast } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
  
  errors: any[] = [];
  loginForm: FormGroup;
  usuario: usuario;
  

  constructor(private fb:FormBuilder,private contaService:contaService,private router:Router,private toastr:ToastrService) 
  {
    this.validationMessages = {
      email: {
        required: 'Informe o e-mail',
        email: 'Email inválido'
      },
      password: {
        required: 'Informe a senha',
        rangeLength: 'A senha deve possuir entre 6 e 15 caracteres'
      }
    };  
    this.genericValidator = new GenericValidator(this.validationMessages);
  }
  
  validationMessages: ValidationMessages;
  genericValidator: GenericValidator;
  displayMessage: DisplayMessage ={};
  
  ngAfterViewInit(): void {
    let controlBlurs: Observable<any>[] = this.formInputElements
    .map((formControl: ElementRef) => fromEvent(formControl.nativeElement, 'blur'));

     merge(...controlBlurs).subscribe(() => {
    this.displayMessage = this.genericValidator.processarMensagens(this.loginForm);
    
  });
  }

  ngOnInit(): void {

     
    this.loginForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password:[Validators.required, CustomValidators.rangeLength([6, 15])]
      })
  }

  login()
  {
    if (this.loginForm.dirty && this.loginForm.valid)
    {
        this.usuario = Object.assign({},this.usuario,this.loginForm.value) ;
        
        this.contaService.login(this.usuario).subscribe(
          sucesso => {this.processarSucesso(sucesso)},
          falha => {this.processarFalha(falha)}
        );
        
    }
  }

  processarSucesso(response: any) {
    this.loginForm.reset();
    this.errors = [];

    this.contaService.LocalStorage.salvarDadosLocaisUsuario(response);

    this.router.navigate(['/home']);

    let toast = this.toastr.success('Login Sucesso!', 'Bem vindo!!!',{timeOut:1000});
    if(toast){
      toast.onHidden.subscribe(() => {
        this.router.navigate(['/home']);
      });
    }
  }

  processarFalha(fail: any){
    this.errors = fail.error.errors;
    this.toastr.error('Ocorreu um erro!', 'Ops');
  }
    



}
