import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LocalStorageUtils } from '../utils/localstorage';
import { Route, Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor
{

    private LocalStorage = new LocalStorageUtils();    
     
    constructor(private router: Router)
    {

    }

    intercept(req: HttpRequest<any>, 
              next: HttpHandler): Observable<HttpEvent<any>> 
    {
        
        return next.handle(req).
        pipe(
            catchError(error =>  
                {
                    if (error instanceof HttpErrorResponse )
                    {
                        if (error.status === 500)
                        {
                            this.LocalStorage.limparDadosLocaisUsuario();
                            this.router.navigate(['conta/login']);
                        }

                        if (error.status === 401)
                        {
                            this.LocalStorage.limparDadosLocaisUsuario();
                            this.router.navigate(['conta/login']);
                        }
                        if (error.status === 403)
                        {
                            this.router.navigate(['acesso-negado']);
                        }
                    }
                    return throwError(error);
                }   
                )
        )

    }

}
